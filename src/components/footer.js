import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome'
import '../App.css';

class Footer extends Component{
    render(){
        return(
            <footer>
                <div className="Copy">
                    <span> <FontAwesome name="copyright"/> 2008 – 2018 Groza.cc</span> <a>Contact us</a>
                </div>
                <div className="Social" >
                    <FontAwesome name='instagram'/>
                    <FontAwesome name='facebook'/>
                    <FontAwesome name='telegram'/>
                    <FontAwesome name='vk'/>
                    <FontAwesome name='behance'/>

                </div>
            </footer>
        )
    }
}

export default Footer;