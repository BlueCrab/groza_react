// import React, { Component } from 'react';
// import FontAwesome from 'react-fontawesome'
// import logo from './logo.svg';
// import './App.css';
//
//
// class Logo extends Component {
//     render(){
//         return(
//             <div className="Logo"><img src={logo} alt="logo" /> <span>GROZA</span></div>
//         )
//     }
// }
//
// class Nav extends Component {
//     render(){
//         return(
//             <div className="Nav"> <ul>
//                 <li>Design</li>
//                 <li>Apps</li>
//                 <li>Code</li>
//                 <li>SEO</li>
//             </ul></div>
//         )
//     }
// }
//
// class Menu extends Component{
//     render(){
//         return(
//             <div className="Menu">
//                 <span>Menu</span> <FontAwesome name='bars' size='2x'/>
//             </div>
//         )
//     }
// }
//
// class Header extends Component{
//     render(){
//         return(
//             <header className="Header">
//                 <Logo/>
//                 <Nav/>
//                 <Menu/>
//             </header>
//         )
//     }
// }
//
// class Footer extends Component{
//     render(){
//         return(
//             <footer>
//                 <div className="Copy">
//                 <span> <FontAwesome name="copyright"/> 2008 – 2018 Groza.cc</span> <a>Contact us</a>
//                 </div>
//                 <div className="Social" >
//                     <FontAwesome name='instagram'/>
//                     <FontAwesome name='facebook'/>
//                     <FontAwesome name='telegram'/>
//                     <FontAwesome name='vk'/>
//                     <FontAwesome name='behance'/>
//
//                 </div>
//             </footer>
//         )
//     }
// }
//
// class Content extends Component{
//     render(){
//         return(
//             <main>
//
//             </main>
//         )
//     }
// }
//
// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//           <Header/>
//           <Content/>
//           <Footer/>
//       </div>
//     );
//   }
// }
//
// export default App;
