import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome'
import logo from '../logo.png'
import '../App.css';

class Logo extends Component {
    render(){
        return(
            <div className="Logo"><img src={logo} alt="logo" /></div>
        )
    }
}

class Nav extends Component {
    render(){
        return(
            <div className="Nav"> <ul>
                <li>Design</li>
                <li>Apps</li>
                <li>Code</li>
                <li>SEO</li>
            </ul></div>
        )
    }
}

class Menu extends Component{
    render(){
        return(
            <div className="Menu">
                <span>Menu</span> <FontAwesome name='bars' size='2x'/>
            </div>
        )
    }
}

class Header extends Component{
    render(){
        return(
            <header className="Header">
                <Logo/>
                <Nav/>
                <Menu/>
            </header>
        )
    }
}

export default Header;