import React, { Component } from 'react';
import thunder from './thunder'

class Content extends Component{
    render(){
        return(
            <main>
                <div className="quote">
                    <p>We can not solve our problems with same level of thinking that created them</p>
                </div>
                <div className="thunder">
                    <canvas id="canvas1" />
                    <canvas id="canvas2" />
                </div>
                <script src={thunder} />

            </main>
        )
    }
}

export default Content;
